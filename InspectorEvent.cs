﻿using UnityEngine;

[System.Serializable]
public struct InspectorListener
{
    [SerializeField] private string _listenerName;
    [SerializeField] private int _order;

    public string ListenerName => _listenerName;
    public int Order => _order;
    
    public static bool operator ==(InspectorListener inspectorListener, Manager_Listeners.IListener _listener)
    {
        if (_listener == null)
            return false;

        return inspectorListener.ListenerName == _listener.GetListenerName();
    }

    public static bool operator !=(InspectorListener inspectorListener, Manager_Listeners.IListener _listener) =>
        !(inspectorListener == _listener);
    
    public static bool operator ==(Manager_Listeners.IListener _listener, InspectorListener inspectorListener) =>
        inspectorListener == _listener;
    
    public static bool operator !=(Manager_Listeners.IListener _listener, InspectorListener inspectorListener) =>
        !(inspectorListener == _listener);
}