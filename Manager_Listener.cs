﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public static class Manager_Listeners
{
    #region Listeners structure
   
    public interface IListener
    {
        public void Register(Action<object[]> callback, int order);

        public void SetListenerName(string name);

        public string GetListenerName();

        public void Trigger(params object[] parameters);
    }
   
    public class Listener : IListener
    {
        private SortedList<int, List<Action>> _callbackDic = new SortedList<int, List<Action>>();

        private string _ListenerName;

        public void SetListenerName(string name)
        {
            if(_ListenerName != null)
                return;

            _ListenerName = name;
        }

        public string GetListenerName() => _ListenerName;

        public void Register(Action<object[]> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Add(() => callback(null));
            else
                _callbackDic.Add(order, new List<Action>() {() => callback(null)});
        }

        public Listener Remove(Action callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Remove(callback);

            return this;
        }
      
        public Listener Trigger()
        {
            for (int i = 0; i < _callbackDic.Count; i++)
            {
                for (var j = 0; j < _callbackDic[i].Count; j++)
                {
                    var callback = _callbackDic[i][j];
                    callback?.Invoke();
                }
            }

            return this;
        }

        public void Trigger(params object[] parameters) => Trigger();
      
        public static Listener operator + (Listener ev, Action callback)
        {
            ev.Register(objs => callback());
            return ev;
        }

        public static Listener operator - (Listener ev, Action callback) => ev.Remove(callback);
    }
   
    public class Listener<T> : IListener 
    {
        private SortedList<int, List<Action<T>>> _callbackDic = new SortedList<int, List<Action<T>>>();
      
        private string _ListenerName;

        public void SetListenerName(string name)
        {
            if(_ListenerName != null)
                return;

            _ListenerName = name;
        }

        public string GetListenerName() => _ListenerName;

        public void Register(Action<object[]> callback, int order = 0) => Register(obj => callback?.Invoke(obj), order);

        public Listener<T> Register(Action<T> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Add(callback);
            else
                _callbackDic.Add(order, new List<Action<T>>() {callback});

            return this;
        }

        public Listener<T> Remove(Action<T> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Remove(callback);

            return this;
        }
      
        public Listener<T> Trigger(T obj)
        {
            for (int i = 0; i < _callbackDic.Count; i++)
            {
                for (var j = 0; j < _callbackDic[i].Count; j++)
                {
                    var callback = _callbackDic[i][j];
                    callback?.Invoke(obj);
                }
            }

            return this;
        }
        
        public void Trigger(params object[] parameters) => Trigger((T)parameters[0]);
      
        public static Listener<T> operator + (Listener<T> ev, Action<T> callback) => ev.Register(callback);
      
        public static Listener<T> operator - (Listener<T> ev, Action<T> callback) => ev.Remove(callback);
    }
   
    public class Listener<T1, T2> : IListener 
    {
        private SortedList<int, List<Action<T1, T2>>> _callbackDic = new SortedList<int, List<Action<T1, T2>>>();
      
        private string _ListenerName;

        public void SetListenerName(string name)
        {
            if(_ListenerName != null)
                return;

            _ListenerName = name;
        }

        public string GetListenerName() => _ListenerName;

        public void Register(Action<object[]> callback, int order = 0) =>
            Register((obj1, obj2) => callback?.Invoke(new object[]{obj1, obj2}), order);

        public Listener<T1, T2> Register(Action<T1, T2> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Add(callback);
            else
                _callbackDic.Add(order, new List<Action<T1, T2>>() {callback});

            return this;
        }

        public Listener<T1, T2> Remove(Action<T1, T2> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Remove(callback);

            return this;
        }
      
        public Listener<T1, T2> Trigger(T1 obj1, T2 obj2)
        {
            for (int i = 0; i < _callbackDic.Count; i++)
            {
                for (var j = 0; j < _callbackDic[i].Count; j++)
                {
                    var callback = _callbackDic[i][j];
                    callback?.Invoke(obj1, obj2);
                }
            }

            return this;
        }
        
        public void Trigger(params object[] parameters) => Trigger((T1)parameters[0], (T2)parameters[1]);

      
        public static Listener<T1, T2> operator + (Listener<T1, T2> ev, Action<T1, T2> callback) => ev.Register(callback);
      
        public static Listener<T1, T2> operator - (Listener<T1, T2> ev, Action<T1, T2> callback) => ev.Remove(callback);
    }
   
    public class Listener<T1, T2, T3> : IListener 
    {
        private SortedList<int, List<Action<T1, T2, T3>>> _callbackDic = new SortedList<int, List<Action<T1, T2, T3>>>();
      
        private string _ListenerName;

        public void SetListenerName(string name)
        {
            if(_ListenerName != null)
                return;

            _ListenerName = name;
        }

        public string GetListenerName() => _ListenerName;

        public void Register(Action<object[]> callback, int order = 0) =>
            Register((obj1, obj2, obj3) => callback?.Invoke(new object[]{obj1, obj2, obj3}), order);


        public Listener<T1, T2, T3> Register(Action<T1, T2, T3> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Add(callback);
            else
                _callbackDic.Add(order, new List<Action<T1, T2, T3>>() {callback});

            return this;
        }

        public Listener<T1, T2, T3> Remove(Action<T1, T2, T3> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Remove(callback);

            return this;
        }
      
        public Listener<T1, T2, T3> Trigger(T1 obj1, T2 obj2, T3 obj3)
        {
            for (int i = 0; i < _callbackDic.Count; i++)
            {
                for (var j = 0; j < _callbackDic[i].Count; j++)
                {
                    var callback = _callbackDic[i][j];
                    callback?.Invoke(obj1, obj2, obj3);
                }
            }

            return this;
        }
        
        public void Trigger(params object[] parameters) => Trigger((T1)parameters[0], (T2)parameters[1], (T3)parameters[2]);

      
        public static Listener<T1, T2, T3> operator + (Listener<T1, T2, T3> ev, Action<T1, T2, T3> callback) => ev.Register(callback);
      
        public static Listener<T1, T2, T3> operator - (Listener<T1, T2, T3> ev, Action<T1, T2, T3> callback) => ev.Remove(callback);
    }
   
    public class Listener<T1, T2, T3, T4> : IListener 
    {
        private SortedList<int, List<Action<T1, T2, T3, T4>>> _callbackDic = new SortedList<int, List<Action<T1, T2, T3, T4>>>();
      
        private string _ListenerName;

        public void SetListenerName(string name)
        {
            if(_ListenerName != null)
                return;

            _ListenerName = name;
        }

        public string GetListenerName() => _ListenerName;

        public void Register(Action<object[]> callback, int order = 0) =>
            Register((obj1, obj2, obj3, obj4) => callback?.Invoke(new object[]{obj1, obj2, obj3, obj4}), order);
      
        public Listener<T1, T2, T3, T4> Register(Action<T1, T2, T3, T4> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Add(callback);
            else
                _callbackDic.Add(order, new List<Action<T1, T2, T3, T4>>() {callback});

            return this;
        }

        public Listener<T1, T2, T3, T4> Remove(Action<T1, T2, T3, T4> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Remove(callback);

            return this;
        }
      
        public Listener<T1, T2, T3, T4> Trigger(T1 obj1, T2 obj2, T3 obj3, T4 obj4)
        {
            for (int i = 0; i < _callbackDic.Count; i++)
            {
                for (var j = 0; j < _callbackDic[i].Count; j++)
                {
                    var callback = _callbackDic[i][j];
                    callback?.Invoke(obj1, obj2, obj3, obj4);
                }
            }

            return this;
        }
        
        public void Trigger(params object[] parameters) => Trigger((T1)parameters[0], (T2)parameters[1], (T3)parameters[2], (T4)parameters[3]);

      
        public static Listener<T1, T2, T3, T4> operator + (Listener<T1, T2, T3, T4> ev, Action<T1, T2, T3, T4> callback) => ev.Register(callback);
      
        public static Listener<T1, T2, T3, T4> operator - (Listener<T1, T2, T3, T4> ev, Action<T1, T2, T3, T4> callback) => ev.Remove(callback);
    }

    public static void Register(InspectorListener inspectorEv, Action<object[]> callback)
    {
        if (TryGetListener(inspectorEv, out var ev))
            ev.Register(callback, inspectorEv.Order);
    }

    public static bool TryGetListener(InspectorListener inspectorEv, out IListener ev) 
    {
        ev = default;
        if (!InspectorExtension.TryGetField(inspectorEv, out var fieldInfo))
        {
            if (Application.isPlaying)
            {
                Debug.LogError($"Not find the Listener {inspectorEv.ListenerName}");
            }

            return false;
        }
        
        ev = (IListener) fieldInfo.GetValue(null);
        
        return ev != null;
    }
   
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
    private static void Setup() 
    {
        foreach (var eListener in InspectorExtension.ValidListenerList)
        {
            var obj = System.Activator.CreateInstance(eListener.Value.FieldType);
            eListener.Value.SetValue(null, obj);
            ((IListener)obj).SetListenerName(eListener.Key);
        }
    }
   
    #endregion

    #region Extension

    public static class InspectorExtension
    { 
        public static readonly Type MainType = typeof(EventList);
    
        private static Dictionary<string, FieldInfo> _ListenerList;

        public static Dictionary<string, FieldInfo> ValidListenerList
        {
            get
            {
                if(_ListenerList == null)
                    Setup();
                return _ListenerList;
            }
        }
        public static void Setup()
        {
            _ListenerList = new Dictionary<string, FieldInfo>();
            IterateInType(MainType);
        }

        public static bool TryGetField(InspectorListener inspectorListener, out FieldInfo eListener)
        {
            eListener = null;
            
            if (_ListenerList == null)
                return false;

            if (inspectorListener == null || inspectorListener.ListenerName == null)
                return false;
            
            if (!_ListenerList.ContainsKey(inspectorListener.ListenerName))
                return false;

            eListener = _ListenerList[inspectorListener.ListenerName];
            
            return true;
        }

        private static void IterateInType(Type type)
        {
            Debug.Log(type.ToString());
            foreach (var field in type.GetFields())
            {
                if(!typeof (IListener).IsAssignableFrom(field.FieldType))
                    continue;

                try
                {
                    var obj = field.GetValue(null);

                    if (obj == null)
                    {
                        field.SetValue(null, Activator.CreateInstance(field.FieldType));
                    }
                }
                catch(Exception e) { }

                _ListenerList.Add($"{type.ToString().Replace("+", "/")}/{field.Name}", field);
            }

            foreach (var nextType in type.GetNestedTypes())
            {
                if (!CanHaveListener(nextType))
                    continue;
                IterateInType(nextType);
            }
        }

        private static bool CanHaveListener(Type type)
        {
            if (typeof(IListener).IsAssignableFrom(type))
                return false;
            if (type == typeof(InspectorExtension))
                return false;
            return true;
        }
    }

    #endregion
}