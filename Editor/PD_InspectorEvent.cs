﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System.Reflection;

[CustomPropertyDrawer(typeof(InspectorListener))]
public class PD_InspectorListener : PropertyDrawer
{
    private const string variableName = "_listenerName";
    
    private readonly float labelWidth = EditorGUIUtility.labelWidth;
    private readonly float space = EditorGUIUtility.standardVerticalSpacing;


    private Color missingEnumColor = new Color(1, 0, 0, .2f);
    
    public override bool CanCacheInspectorGUI(SerializedProperty property)
    {
        return false;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        Rect rectButton = position;
        
        if (position.width > labelWidth + 100)
        {
            Rect rectName = position;
            rectName.width = labelWidth;
            
           
            rectButton.width = position.width - rectName.width - space;
            rectButton.x = position.width - rectButton.width + space + 11;
            
            EditorGUI.LabelField(rectName, Variable().displayName);
        }

        string eventName = Variable().stringValue;
        string tooltip = "Select an event";

        bool hasName = true;

        if (string.IsNullOrEmpty(eventName))
        {          
            eventName = "Null";
            hasName = false;
        }
        else
        {
            eventName = eventName.Remove(0, Manager_Listeners.InspectorExtension.MainType.ToString().Length + 1);
            tooltip = eventName.Replace("/", " > ");
            var paths = eventName.Split('/');
            eventName = "";
            for (int i = Mathf.Max(paths.Length - 2, 0); i < paths.Length; i++)
            {
                eventName += $"{paths[i]}.";
            }

            eventName = eventName.Substring(0,eventName.Length - 1);
        }
        
      
        if (GUI.Button(rectButton, new GUIContent(eventName, tooltip)))
        {
            DrawMenu(property, Variable());
        }
        
        if(!hasName)
            EditorGUI.DrawRect(position, missingEnumColor);
        
        SerializedProperty Variable() => property.FindPropertyRelative(variableName);
    }
    
    private void DrawMenu(SerializedProperty property, SerializedProperty variable)
    {
        GenericMenu menu = new GenericMenu();

        var _enumList = Manager_Listeners.InspectorExtension.ValidListenerList;

        int index = 1;
        
        menu.AddItem(new GUIContent("Null"), false, () =>
        {
            variable.stringValue = "";
            EditorUtility.SetDirty(property.serializedObject.targetObject);
            property.serializedObject.ApplyModifiedProperties();
        });
        
        foreach (var info in _enumList)
        {
            index++;
            string itemName = info.Key.Remove(0, Manager_Listeners.InspectorExtension.MainType.ToString().Length + 1);
            menu.AddItem(new GUIContent(itemName), false, () =>
            {
                variable.stringValue = index > 0 ? info.Key : "";
                EditorUtility.SetDirty(property.serializedObject.targetObject);
                property.serializedObject.ApplyModifiedProperties();
            });
        }

        menu.ShowAsContext();
    }
}