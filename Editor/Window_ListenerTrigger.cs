using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class Window_ListenerTrigger : EditorWindow
{
    class EventArguments
    {
        public object _obj;
        public Type _type;

        public EventArguments(object obj, Type type)
        {
            _obj = obj;
            _type = type;
        }
    }
    
    class DrawTypes
    {
        public delegate object Draw(string label, object obj);
        public Type _type;
        public Draw _drawAction;
        public object _defaultValue;

        public DrawTypes(Type type, object defaultValue, Draw drawAction)
        {
            _type = type;
            _drawAction = drawAction;
            _defaultValue = defaultValue;
        }
    }
    
    public InspectorListener _listener;
    private List<EventArguments> _eventArguments;
    private SerializedProperty _listenerProperty;
    private SerializedObject _so;
    private string _currentEventName;
    private bool _isValidEvent;
    private Manager_Listeners.IListener _eventObject;

    private List<DrawTypes> _drawList = new List<DrawTypes>()
    {
        new (typeof(int),            0,                    (label, obj) => EditorGUILayout.IntField        (label, (int)obj)),
        new (typeof(long),           0,                    (label, obj) => EditorGUILayout.LongField       (label, (long)obj)),
        new (typeof(float),          0.0f,                 (label, obj) => EditorGUILayout.FloatField      (label, (float)obj)),
        new (typeof(double),         0.0,                  (label, obj) => EditorGUILayout.DoubleField     (label, (double)obj)),
        new (typeof(bool),           false,                (label, obj) => EditorGUILayout.Toggle          (label, (bool)obj)),
        new (typeof(string),         string.Empty,         (label, obj) => EditorGUILayout.TextField       (label, (string)obj)),
        new (typeof(Color),          Color.white,          (label, obj) => EditorGUILayout.ColorField      (label, (Color)obj)),
        new (typeof(Bounds),         new Bounds(),         (label, obj) => EditorGUILayout.BoundsField     (label, (Bounds)obj)),
        new (typeof(AnimationCurve), new AnimationCurve(), (label, obj) => EditorGUILayout.CurveField      (label, (AnimationCurve)obj)),
        new (typeof(Rect),           new Rect(),           (label, obj) => EditorGUILayout.RectField       (label, (Rect)obj)),
        new (typeof(BoundsInt),      new BoundsInt(),      (label, obj) => EditorGUILayout.BoundsIntField  (label, (BoundsInt)obj)),
        new (typeof(RectInt),        new RectInt(),        (label, obj) => EditorGUILayout.RectIntField    (label, (RectInt)obj)),
        new (typeof(Vector2Int),     new Vector2Int(),     (label, obj) => EditorGUILayout.Vector2IntField (label, (Vector2Int)obj)),
        new (typeof(Vector3Int),     new Vector3Int(),     (label, obj) => EditorGUILayout.Vector3IntField (label, (Vector3Int)obj)),
        new (typeof(Vector2),        new Vector2(),        (label, obj) => EditorGUILayout.Vector2Field    (label, (Vector2)obj)),
        new (typeof(Vector3),        new Vector3(),        (label, obj) => EditorGUILayout.Vector3Field    (label, (Vector3)obj)),
        new (typeof(Vector4),        new Vector4(),        (label, obj) => EditorGUILayout.Vector4Field    (label, (Vector4)obj))
    };
    

    [MenuItem("Window/Listeners Trigger")]
    private static void Setup()
    {
        Window_ListenerTrigger window = (Window_ListenerTrigger) GetWindow(typeof(Window_ListenerTrigger));
       
        window.Show();
    }

    private void OnEnable()
    {
        _listener = new InspectorListener();
        _so = new SerializedObject(this);
        _listenerProperty = _so.FindProperty("_listener");
        _eventArguments = new List<EventArguments>();
        _currentEventName = string.Empty;
    }

    private void OnGUI()
    {
        CheckEventChanged();
        DrawScreen();
    }

    private void CheckEventChanged()
    {
        if (_listener == null)
        {
            if (!string.IsNullOrEmpty(_currentEventName))
            {
                _currentEventName = string.Empty;
                OnEventChange();
            }

            return;
        }

        if (_listener.ListenerName == _currentEventName)
            return;
        
        _currentEventName = _listener.ListenerName;
        OnEventChange();
    }

    private void OnEventChange()
    {
        _eventArguments.Clear();
        _isValidEvent = TryGetEventObject(out _eventObject);
        if(_eventObject == null)
            return;
        foreach (var genericTypeArgument in _eventObject.GetType().GenericTypeArguments)
        {
            _eventArguments.Add(new EventArguments(null, genericTypeArgument));
        }
    }

    private void DrawScreen()
    {
        EditorGUILayout.PropertyField(_listenerProperty);
        DrawEventArguments();
        GUILayout.FlexibleSpace();
        EditorGUI.BeginDisabledGroup(!_isValidEvent);
        if(GUILayout.Button("Invoke"))
        {
            object[] arguments = new object[_eventArguments.Count];
            for (int i = 0; i < _eventArguments.Count; i++)
            {
                arguments[i] = _eventArguments[i]._obj;
            }
            _eventObject.Trigger(arguments);
        }
    }

    private void DrawEventArguments()
    {
        int count = 0;
        foreach (var eventArgument in _eventArguments)
        {
            Draw(eventArgument, count);
            count++;
        }

        void Draw(EventArguments arguments, int count)
        {
            var type = arguments._type;
            var obj = arguments._obj;
            var drawer = _drawList.Find(x => x._type == type);
            var label = $"Argument {count} ({type.Name})";

            if (drawer != null)
            {
                if (obj == null)
                {
                    obj = drawer._defaultValue;
                }
                obj = drawer._drawAction(label, obj);
            }
            else
            {
                DrawException();
            }

            arguments._obj = obj;

            void DrawException()
            {
                if (typeof(Object).IsAssignableFrom(type))
                {
                    obj = EditorGUILayout.ObjectField (label, (Object) obj, type, true);
                    return;
                }
                
                if (typeof(Enum).IsAssignableFrom(type) && type != typeof(Enum))
                {
                    if (obj == null)
                    {
                        obj = Enum.GetValues(type).GetValue(0);
                    }

                    obj = EditorGUILayout.EnumPopup(label, (Enum) obj);
                    return;
                }

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(label, GUILayout.Width(EditorGUIUtility.labelWidth));
                EditorGUILayout.LabelField($"Cant send an {type.Name} type");
                EditorGUILayout.EndHorizontal();
            }
        }
    }

    private bool TryGetEventObject(out Manager_Listeners.IListener ev)
    {
        ev = null;
        if (_listener == null)
            return false;
        if (string.IsNullOrEmpty(_listener.ListenerName))
            return false;
        if (!Manager_Listeners.TryGetListener(_listener, out ev))
            return false;
        return true;
    }

    private Dictionary<string, FieldInfo> GetList()
    {
        return Manager_Listeners.InspectorExtension.ValidListenerList;
    }
    
}